const request = require('superagent')

module.exports = function (signupServiceUrl) {
    return {
        getBar: function () {
            return new Promise( (resolve, reject) => {
                request
                .get(`${signupServiceUrl}/api/bar`)
                .then(
                    (response) => {
                        resolve(response.body)
                    },
                    (error) => reject(error)
                )
            })
        }
    }
}