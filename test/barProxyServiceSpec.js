const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const expect = chai.expect
const { Pact } = require('@pact-foundation/pact')
const BarProxyService = require('../src/service/barProxyService');
const MOCK_SERVER_PORT = 6666
const LOG_LEVEL = process.env.LOG_LEVEL || 'WARN'

chai.use(chaiAsPromised);

describe('BarProxyService', () => {
    const provider = new Pact({
        consumer: 'Bar proxy Service',
        provider: 'Sign up Service',
        port: MOCK_SERVER_PORT,
        log: path.resolve(process.cwd(), 'logs', 'mockserver-integration.log'),
        dir: path.resolve(process.cwd(), 'pacts'),
        logLevel: LOG_LEVEL,
        spec: 2
    })

    before(() => provider.setup())

    afterEach(() => provider.verify());

    after(() => provider.finalize())

    const barProxyService = new BarProxyService(`http://localhost:${MOCK_SERVER_PORT}`)

    describe('when bar is available', () => {
        const barBodyExpectation = {
            "bar": "bar"
        }

        before(() => {
            const interaction = {
                state: 'Has bar',
                uponReceiving: 'a request for bar',
                withRequest: {
                    method: 'GET',
                    path: '/api/bar'
                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: barBodyExpectation
                }
            }
            return provider.addInteraction(interaction)
        })

        it('should return bar', (done) => {
            const bar = barProxyService
                .getBar()
                .then(
                    bar => bar,
                    () => null
                )

            expect(bar).to.eventually.have.deep.property('bar', 'bar').notify(done)
        });
    })

    describe('When bar is not available', () => {
        before(() => {
            const interaction = {
                state: 'Has no bar',
                uponReceiving: 'a request for bar',
                withRequest: {
                    method: 'GET',
                    path: '/api/bar'
                },
                willRespondWith: {
                    status: 404
                }
            }
            return provider.addInteraction(interaction)
        })

        it('should return not found', (done) => {
            const error = barProxyService
                .getBar()
                .then(
                    () => null,
                    error => error
                )

            expect(error).to.eventually.have.deep.property('status', 404).notify(done);
        });
    })
})