const express = require('express');
const BarProxyService = require('./src/service/barProxyService')

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public'));

const singUpServiceUrl = process.env.SIGN_UP_SERVICE_URL || 'http://localhost:7777'
console.log(`signUpServiceUrl: ${singUpServiceUrl}`)
const barProxyService = new BarProxyService(singUpServiceUrl)

app.get('/barProxy', (request, response) => {
    barProxyService.getBar().then(
        (bar) => {
            response.json(bar);
        },
        (error) => {
            console.log(error)
            response.status(error.status).send(error.body);
        }).catch( () => {
            response.status(500).end();
        })
});

app.listen('1111', () => {
    console.log('Proxy Server Start');
})